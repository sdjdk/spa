package com.example.salonapi.exceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomError {
    private String errorMsg;
    private Integer errorCode;
    private Long errorTime;

}
