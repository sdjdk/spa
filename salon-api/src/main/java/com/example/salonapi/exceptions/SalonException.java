package com.example.salonapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class SalonException extends RuntimeException {
    public SalonException() {
        super();
    }

    public SalonException(String message) {
        super(message);
    }

    public SalonException(String message, Throwable cause) {
        super(message, cause);
    }

    public SalonException(Throwable cause) {
        super(cause);
    }

}
