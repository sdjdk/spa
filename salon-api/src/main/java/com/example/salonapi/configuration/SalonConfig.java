package com.example.salonapi.configuration;

import com.example.salonapi.SalonDetails;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SalonConfig {

    @Bean
    @ConfigurationProperties(prefix="com.example.salonapi.salon")
    public SalonDetails getSalonDetails() {
        return new SalonDetails();
    }
}
