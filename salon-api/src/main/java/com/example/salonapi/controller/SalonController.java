package com.example.salonapi.controller;

import com.example.salonapi.SalonDetails;
import com.example.salonapi.configuration.SalonConfig;
import com.example.salonapi.exceptions.SalonException;
import com.example.salonapi.model.SalonServiceDetail;
import com.example.salonapi.model.Slot;
import com.example.salonapi.repositories.SalonServiceDetailRepository;
import com.example.salonapi.repositories.SlotRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("")
@AllArgsConstructor
public class SalonController {

    private final SalonServiceDetailRepository salonServiceDetailRepository;
    private final SlotRepository slotRepository;
   // private final SalonDetails salonDetails;
    private final SalonConfig salonConfig;

    @GetMapping("/SalonServiceDetail/{id}")
    public ResponseEntity<SalonServiceDetail> findOneSalonServiceDetail(@PathVariable Long id) throws SalonException {
        return salonServiceDetailRepository.findById(id)
                .map(e-> new ResponseEntity<>(e, HttpStatus.OK))
                .orElseThrow(()->new SalonException(String.format("Unable to find Salon Service Details for id %s ",id)));
    }

    @GetMapping("/SalonServiceDetail")
    public ResponseEntity<List<SalonServiceDetail>> showAllSalonServiceDetail() {
        return ResponseEntity.ok(salonServiceDetailRepository.findAll());
    }

    @GetMapping("/Slot")
    public ResponseEntity<List<Slot>> showAllSlot() {
        return ResponseEntity.ok(slotRepository.findAll());
    }

    @GetMapping("/Slot/{id}")
    public ResponseEntity<Slot> findOneSlot(@PathVariable Long id) {
        return slotRepository.findById(id)
                .map(e-> new ResponseEntity<>(e, HttpStatus.OK))
                .orElseThrow(()->new SalonException(String.format("Unable to find Slot Details for id %s ",id)));
    }

    @GetMapping("details")
    public ResponseEntity<SalonDetails> getDetails() {
        return ResponseEntity.ok(salonConfig.getSalonDetails());
    }
}
