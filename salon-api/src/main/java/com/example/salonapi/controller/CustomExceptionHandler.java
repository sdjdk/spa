package com.example.salonapi.controller;

import com.example.salonapi.exceptions.CustomError;
import com.example.salonapi.exceptions.SalonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SalonException.class)
    public ResponseEntity<CustomError> customErrorHandler(Exception exception, HttpServletResponse response) {

        CustomError customError = CustomError.builder().errorCode(500).errorMsg(exception.getMessage()).errorTime(System.currentTimeMillis()).build();

        return new ResponseEntity<>(customError, HttpStatus.NOT_FOUND);
    }

}
