package com.example.salonapi;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SalonDetails {
    private String name;
    private String address;
    private String city;
    private String state;
    private String zipcode;
    private String phone;
}
