import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

class App extends Component {
  render () {
  return (
  <Router>
    <header>
         <div class="navbar navbar-dark bg-dark box-shadow">
                 <div class="container d-flex justify-content-between">
                   <a href="/#" class="navbar-brand d-flex align-items-center">
                   <strong>AR Salon and Day Spa Services</strong>
                   </a>
                 </div>
         </div>
    </header>
      <main role="main">
        <Switch>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </main>
    </Router>
  );
  }
}

function Home() {
  return <section class="jumbotron text-center  bg-white text-black">
          <div class="container">
                   To get started, edit <code>src/App.js</code> and save to reload.
             </div>
                   </section> ;
}

export default App;